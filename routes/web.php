<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['status','auth']], function () {
    Route::get('/admin', 'AdminController@admin');

});


Route::post('/admin', 'AdminController@postAdd');
Route::post('/adminsciene', 'AdminController@scieneAdd');
Route::post('/adminscienedel', 'AdminController@scieneDelete');
Route::post('/adminnumberadd', 'AdminController@NamberUserAdd');
Route::post('/adminnumberdelete', 'AdminController@NamberUserDelete');
Route::post('/search', 'UserSearchController@search');
Route::post('/BookDelete', 'AdminController@BookDelete');
Route::get('/lifesearch', 'UserSearchController@lifesearch');
Route::post('/updatebook', 'AdminController@UpdateBook');
/*Route::get('/user_page', 'UserSearchController@index')->name("user_page");
Route::post('/user_page', 'AdminController@index');

Route::get( '/search_result', 'UserSearchController@search');*/
Route::get('/download/{id}', 'UserSearchController@download_fil');
Route::get('/scinelist', 'AdminController@admin');


Route::get('/', 'HomeController@index');

/*
 * Авторизация
*/
Auth::routes();





