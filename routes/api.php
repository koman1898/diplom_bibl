<?php

use Illuminate\Support\Facades\Route;




Route::get('', 'ProductsController@index');

Route::get('products/{product}', 'ProductsController@show');

Route::post('books','AdminController@create');

Route::put('products/{product}','ProductsController@update');

Route::delete('products/{product}', 'ProductsController@delete');

