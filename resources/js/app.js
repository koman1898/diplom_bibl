/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */


require('./bootstrap');
window.Vue = require('vue');


window.VueRouter=require('vue-router').default;

window.VueAxios=require('vue-axios').default;

window.Axios=require('axios').default;


/*let AppLayout= require('./components/ExampleComponent.vue');
/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/*Vue.component('example-component', require('./components/ExampleComponent.vue').default);

const app = new Vue({
    el: '#app',
});
Vue.component('Admin', require('./components/Admin.vue').default);
*/
Vue.use(VueRouter)

import Admin from './components/Admin.vue';
import SearchV from './components/SearchV.vue';
import ExampleComponent from './components/ExampleComponent.vue';
import DeleteBook from './components/DeleteBook.vue';
import UpdateBook from './components/UpdateBook.vue';
import AddBook from './components/AddBook.vue';
import EditUser from './components/EditUser.vue';
import AddSciene from "./components/AddSciene";



const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/admin-page',
            name: 'admin',
            component: Admin,
        },
        {
            path: '/searchV',
            name: 'SearchV',
            component: SearchV,
        },

        {
            path: '/admin-page/DeleteBook',
            name: 'DeleteBook',
            component:  DeleteBook,
        },
        {
            path: '/admin-page/AddBook',
            name: 'AddBook',
            component: AddBook,
        },
        {
            path: '/admin-page/UpdateBook',
            name: 'UpdateBook',
            component:  UpdateBook,
        },
        {
            path: '/admin-page/EditUser',
            name: 'EditUser',
            component:  EditUser,
        },
        {
            path: '/admin-page/AddSciene',
            name: 'AddSciene',
            component:  AddSciene,
        },


    ],
});

const app = new Vue({
    el: '#app',
    components: {ExampleComponent},
    router,
});





