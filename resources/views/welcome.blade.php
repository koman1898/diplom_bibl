<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>



        <title>Bibl PGY</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style >


            body {
                background: url("https://s22.postimg.cc/gzyyouldd/grey-paper-texture.jpg") center top;

        </style>
    </head>
    <body>

    <div className="content" style="

                                                           background-repeat: no-repeat;
                                                           background-position: top right 120px;
                                                           background-attachment: fixed;">

        @if (Route::has('login'))
            <div className=" links" style="width: 100%;


                                                     border:3px  ;
                                                     border-radius: 5px;
                                                     display: flex;
                                                     flex-direction: row;
                                                     justify-content: center;

                                                                    ">
                @auth
             <div style="display: flex;
                        justify-content: center;
                        border: 2px solid #28a745;
                        border-radius:5px;
                        padding: 10px;                        ">
                    <div>
                        <h1 style="color: #28a745;">Библиотека РФ ПГУ</h1>

                    </div>

                    <strong>
                        <a class="badge badge-danger" style="margin-left: 700px;
                                                       width: 80px;
                                                       height: 50px;
                                                       padding-top: 15px;

" href="{{route('logout')}}"
                           onClick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                            Выйти</a>
                    </strong>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                        @csrf
                    </form>
                @else




            </div>
        @endauth
            </div>
        @endif
        </div>

    <div style="align-items: center" id="app">
        <example-component :user="{{\Illuminate\Support\Facades\Auth::user()}}"></example-component>
    </div>
    <script src="{{ mix('/js/app.js') }}" defer></script>


    </body>


</html>


