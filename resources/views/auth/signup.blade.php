@extends('layouts.app')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@section('content')
    <div>
        <h2>Ркгистарция</h2>>
    </div>>
    <form method="POST" action="{{route('auth.signup')}}">
        @csrf
        <div class="form-group">
            <label for="email">Email </label>
            <input type="email" name="email" class="form-control" id="email"  placeholder="Введите почту" value="{{\http\Env\Request::old('email') ?: ''}}">
        </div>
        <div class="form-group">
            <label for="password">Пароль</label>
            <input type="password" name="password" class="form-control" id="password" placeholder="Введите пароль">
        </div>
        <div class="form-group">
            <label for="Name">Имя</label>
            <input type="text" name="Name" class="form-control" id="Name" placeholder="Введите имя" value="{{\http\Env\Request::old('Name') ?: ''}}">
        </div>
        <div class="form-group">
            <label for="SerName">Фамилия</label>
            <input type="text" name="SerName" class="form-control" id="SerName" placeholder="Введите фамилию" value="{{\http\Env\Request::old('SerName') ?: ''}}">
        </div>
        <div class="form-group">
            <label for="FatherName">Отчество</label>
            <input type="text" name="FatherName" class="form-control" id="FatherName" placeholder="Введите отчество" value="{{\http\Env\Request::old('FatherName') ?: ''}}">
        </div>
        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Check me out</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
