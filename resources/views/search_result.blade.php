@extends('layouts.app')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@section('content')
    @if (Route::has('login'))
    <div class="container">
        <form action="/user_page"  method="get" >
            <button type="submit" class="btn btn-outline-success">Назад к поиску</button>
        </form>
        @if (!empty($books))
            <div class="nav flex-column">
                <h2>Результаты поиска:</h2>
                @foreach($books as $book)
                    @if(!empty($book))
                        <a class="nav-link pl-5  mb-3" href="/download/{{$book->id}}">{{$book->name_book}}</a>
                        @endif
                    @endforeach
                @endif

            </div>
            @if(empty($book))
                <h2>Книга не найденна</h2>
            @endif

    </div>
    @endif

@endsection
