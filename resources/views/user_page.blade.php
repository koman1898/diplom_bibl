@extends('layouts.app')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@section('content')
    @if (Route::has('login'))
    <form action="/search_result" method="get" >
        <div class="container">
            <div class="input-group md-form form-sm form-2 pl-0">
                <input id="search" name="search" class="form-control my-0 py-1 lime-border" type="text" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button type="submit" class="btn btn-outline-success">поиск</button>
                </div>
            </div>
        </div>
    </form>
    @endif
@endsection
