@extends('layouts.app')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
@section('content')
<div class="container">
    <form method="POST" action="/admin" novalidate enctype="multipart/form-data">
        @csrf
        <div class="card-header">Добавленеие книг</div>
        <div class="input-group mb-3" >
            <div class="input-group-prepend">
                <span class="input-group-text">Название книги</span>
            </div>
            <input type="text" name="name_book" class="form-control" id="name_book" aria-describedby="basic-addon3">
        </div>
        <div class="input-group mb-3" >
            <div class="input-group-prepend">
                <span class="input-group-text">Автор книги</span>
            </div>
            <input type="text" NAME="NameWriters" class="form-control" id="basic-url" aria-describedby="basic-addon3">
        </div>


        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">Науки</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01" name="sciene">
                @for($a= 0; $a <count($scienes); $a++)
                          <option   value=" {{($scienes[$a]->id)}}">{{($scienes[$a]->name_of_scine)}}</option>
                @endfor
            </select>
        </div>
        <div class="input-group mb-3" >
            <div class="input-group-prepend">
                <span class="input-group-text">Дата выпуска</span>
            </div>
            <input type="date"  name="data_vihoda" class="form-control" id="data_vihoda"  placeholder="Дата" required>
        </div>

        <div class="input-group mb-3" >
            <div class="input-group-prepend">
                <span class="input-group-text">Путь к файлу</span>
            </div>
            <div class="custom-file">
                <input type="file" name="root_to_file" class="custom-file-input" id="root_to_file">
                <label class="custom-file-label" for="customFile" placeholder="Файл книги"></label>
            </div>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">описание книги</span>
            </div>
            <textarea class="form-control" name="opisanie" aria-label="With textarea" id="opisanie"></textarea>
        </div>


        <button type="submit" class="btn btn-success" >Добавить книгу</button>
    </form>

</div>
@endsection
