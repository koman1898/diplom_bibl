<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
class Books extends Model
{
    protected $table ='books';
    protected $fillable = array("id_scine","name_book","data_vihoda","opisanie","root_to_file");
}
