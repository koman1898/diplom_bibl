<?php

namespace App;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ListNamberStud extends Model
{
    protected $table ='list_namber_stud';
    protected $fillable = array("namber_stud");

  public function isFolceUser(Request $request){
        $studtest = $request->input('studik');
        $firs=DB::table('list_namber_stud')->where('namber_stud', $studtest)->exists();
       if ($firs) return 'firs';
    }
}
