<?php

namespace App\Http\Controllers;

use App\Writer;
use App\writers_book;
use Illuminate\Http\Request;
use App\Books;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

//use Symfony\Component\Console\Input\Input;
use League\CommonMark\Input;


class UserSearchController extends Controller
{
    private $original;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('user_page');
    }

    public function search(Request $request){

        $query = !empty(trim($request->search)) ? trim($request->search) : null;
       // dd($query);
        $books = DB::table('books')->where('name_book', 'LIKE', '%' . $query . '%')
        ->get();

              return response()->json($books);



    }
    public function lifesearch(Request $request){
        $type = $request->input('type');
        if($type=== '1') {
        $search= $request->input('search');
        $writers = Writer::where('Name', 'LIKE', '%' . $search . '%')->get();

            $colect = collect();
        foreach ($writers as $writer) {
            $id = $writer->id;
            $w_b = writers_book::where('wriner_id',$id)->get();

            foreach ($w_b as $wb) {

                $resultbooks = Books::where('id', $wb->book_id)->get();
                $colect= $colect->merge($resultbooks);

            }

        }

        return response()->json($colect);
    }
        if($type=== '2') {
            $search= $request->input('search');
            $resultbooks = Books::where('name_book', 'LIKE', '%' . $search . '%')->get();
            return response()->json($resultbooks);
        }
        if($type=== '3') {
            $search= $request->input('search');
            $resultbooks = Books::where('name_book', 'LIKE', '%' . $search . '%')->get();
            return response()->json($resultbooks);
        }

    }



    public function download_fil($id){
        $book= Books::where('id', $id)->get();
        $file= $book[0]->root_to_file;
        $f = explode('.', $file);
        $f_end= end($f);
        return Storage::download($book[0]->root_to_file, $book[0]->name_book.'.'. $f_end);
    }
}
