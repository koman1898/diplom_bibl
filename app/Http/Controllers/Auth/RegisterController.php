<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\ListNamberStud;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'SerName' => ['required', 'string', 'max:255'],
            'FatherName' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
//    public function stud_or_bot(Request $request)
//    {
//
//        $stud = ListNamberStud::where('namber_stud', $request->input('namber_stud_register'))->exists();
//
//
//
//        if(!($stud)){
//            return view('auth.register');
//        }
//        else {
//            User::create([
//
//                'name' => $request->input('name'),
//                'email' => $request->input('email'),
//                'SerName'=>$request->input('SerName'),
//                'FatherName'=>$request->input('FatherName'),
//                'number_stud'=>$stud,
//                'password' => Hash::make($request->input('password')),
//            ]);
//        }



//    }

    protected function create(array $data)
    {
    //$stud = ListNamberStud::where('namber_stud', 'namber_stud_register')->exists();


       // if ($stud) {
            $reg = User::create([

                'name' => $data['name'],
                'email' => $data['email'],
                'SerName' => $data['SerName'],
                'FatherName' => $data['FatherName'],
                'number_stud' => $data['namber_stud_register'],
                'password' => Hash::make($data['password']),
            ]);
            return $reg;
//        }
//        else {
//        return view("auth.register");
//    }

}
}
