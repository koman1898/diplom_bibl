<?php

namespace App\Http\Controllers;

use App\ListNamberStud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Books;
use App\Writer;
use App\Scienes;
use Illuminate\Support\Facades\Storage;
use App\writers_book;

class AdminController extends Controller
{


    public function __construstor(){
        $this->middleware('auth');
        $this->middleware('status');
    }

    public function admin(Request $request)
    {

        $item= DB::table('scienes')->get();
        return response()->json($item);


    }



    public function postAdd(Request $request)
    {
        $path =$request->file('root_to_file')->storePublicly('public/BooksFiles');

        Books::create([
         'id_scine'=> $request->input('sciene'),
         'name_book'=> $request->input('name_book'),
         'data_vihoda'=> $request->input('data_vihoda'),
         'opisanie'=> $request->input('opisanie'),
         'root_to_file'=>$path,
         ]);
        $author = $request->input('NameWriters');
        $ss = explode(',',$author);

        foreach ($ss as $s )
        {
         $writer = Writer::where('Name', $s)->exists();
            if($writer == false)
            {
                if ($s !== ''){
                Writer::create([
                    'Name' => $s
                ]);

                }

            }
            $wr=Writer::where('Name', $s)->first();
            $bo= Books::where('name_book', $request->input('name_book'))->first();
            writers_Book::create([
                'wriner_id' => $wr->id,
                'book_id' => $bo->id
            ]);


        }


    }
    public function scieneAdd(Request $request)
    {

        Scienes::create([
            'name_of_scine'=> $request->input('name_of_sciene'),
        ]);
    }

    public function scieneDelete(Request $request)
    {

        $query = $request->input('sciene');
        DB::table('scienes')->where('name_of_scine',  $query )->delete();



    }

    public function NamberUserAdd(Request $request)
    {
        ListNamberStud::create([
            'namber_stud'=> $request->input('number_stud'),
        ]);
    }
    public function NamberUserDelete(Request $request)
    {

        $query = $request->input('number_stud_delete');
        $delstud = DB::table('list_namber_stud')->where('namber_stud',  $query )
            ->delete();
        $delstud2 = DB::table('users')->where('number_stud',  $query )
            ->delete();


    }
    public function BookDelete(Request $request)
    {
        $filename= DB::table('books')->where('id',  $request->input('id'))->pluck('root_to_file');
        Storage::delete($filename[0]);
        $delbook = DB::table('books')->where('id',  $request->input('id') )
            ->delete();
        $stor = new Storage();

    }
    public function  UpdateBook(Request $request) {

            $id = $request->input('id');
            $opisanie = $request->input('opisanine');
            $data_vihoda = $request->input('data_vihoda');
            $name_book = $request->input('name_book');
            $NameWriters = $request->input('NameWriters');
            $sciene = $request->input('sciene');
            $root_to_file = $request->file('root_to_file');


             $new= Books::where('id', $id)->first();
           if ($root_to_file !== null)
           {
                $path =$request->file('root_to_file')->storePublicly('public/BooksFiles');
                Storage::delete($new->root_to_file);
                $new->root_to_file = $path;
           }

        if ($opisanie !== null)
        {
            $new->opisanie= $opisanie;
        }

        if ($data_vihoda !== null)
        {
            $new->data_vihoda= $data_vihoda;
        }
        if ($name_book !== null)
        {
            $new->name_book= $name_book;
        }
        if ($NameWriters !== null)
        {
            $new->NameWriters =$NameWriters;
        }
        if ($sciene !== null)
        {
            $new->sciene =$sciene;
        }

        $new->save();
            return response('ok',200);

    }


}

