<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Books;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Books::class, function (Faker $faker) {
    return [
        'id_scine' => '1',
        'name_book' => $faker->name,
        'data_vihoda'=> $faker->date(),
        'opisanie'=> $faker->text(200, 2),
        'root_to_file'=> 'public/BooksFiles/HNF1UnWhLwQwUK3Juv7MQU1Y6fDF1WjkhjatBFwe.docx'
    ];
});
